#!/usr/bin/env bash

set -eu -o pipefail

sudo -n true
test $? -eq 0 || exit 1 "You should have sudo priveledge to run this script."

printf '%b\n' "> Installing plugins for interception-tools..."
while read -r p ; do sudo xbps-install -y $p ; done < <(cat << "EOF"
    caps2esc
    
EOF
)
printf '%b\n' "> OK"

printf '%b\n' "> Regenerating initramfs..."
dracut --regenerate-all --force || exit
printf '%b\n' "> OK"
